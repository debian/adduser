# adduser program translation
# Copyright (C) (insert year and author here)
# This file is distributed under the same license as the adduser package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: adduser 3.145\n"
"Report-Msgid-Bugs-To: adduser@packages.debian.org\n"
"POT-Creation-Date: 2025-03-06 11:07+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Your Name <your.email@example.com>\n"
"Language-Team: MyProject Team <team@example.com>\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
#: adduser:217
msgid "Only root may add a user or group to the system."
msgstr ""

#: adduser:260 deluser:198
msgid "No options allowed after names."
msgstr ""

#: adduser:269 deluser:206
msgid "Only one or two names allowed."
msgstr ""

#: adduser:276
msgid "Specify only one name in this mode."
msgstr ""

#: adduser:280
msgid "addgroup with two arguments is an unspecified operation."
msgstr ""

#: adduser:312
msgid "The --group, --ingroup, and --gid options are mutually exclusive."
msgstr ""

#: adduser:395
msgid "The home dir must be an absolute path."
msgstr ""

#: adduser:437
#, perl-format
msgid "The group `%s' already exists, but has a different GID. Exiting."
msgstr ""

#: adduser:443
#, perl-format
msgid "The group `%s' already exists as a system group."
msgstr ""

#: adduser:446
#, perl-format
msgid "The group `%s' already exists and is not a system group. Exiting."
msgstr ""

#: adduser:451 adduser:491
#, perl-format
msgid "The GID `%s' is already in use."
msgstr ""

#: adduser:462
#, perl-format
msgid "No GID is available in the range %d-%d (FIRST_SYS_GID - LAST_SYS_GID)."
msgstr ""

#: adduser:464 adduser:510
#, perl-format
msgid "The group `%s' was not created."
msgstr ""

#: adduser:470 adduser:515
#, perl-format
msgid "Adding group `%s' (GID %d) ..."
msgstr ""

#: adduser:487 adduser:1221
#, perl-format
msgid "The group `%s' already exists."
msgstr ""

#: adduser:508
#, perl-format
msgid "No GID is available in the range %d-%d (FIRST_GID - LAST_GID)."
msgstr ""

#: adduser:532 deluser:287
#, perl-format
msgid "The user `%s' does not exist."
msgstr ""

#: adduser:536 adduser:1001 adduser:1231 deluser:447 deluser:450
#, perl-format
msgid "The group `%s' does not exist."
msgstr ""

#: adduser:541 adduser:1005
#, perl-format
msgid "The user `%s' is already a member of `%s'."
msgstr ""

#: adduser:545 adduser:1011
#, perl-format
msgid "Adding user `%s' to group `%s' ..."
msgstr ""

#: adduser:563
#, perl-format
msgid "The user `%s' already exists, but is not a system user. Exiting."
msgstr ""

#: adduser:567
#, perl-format
msgid "The user `%s' already exists with a different UID. Exiting."
msgstr ""

#: adduser:571
#, perl-format
msgid "The system user `%s' already exists. Exiting.\n"
msgstr ""

#: adduser:585
#, perl-format
msgid ""
"No UID/GID pair is available in the range %d-%d (FIRST_SYS_UID - "
"LAST_SYS_UID)."
msgstr ""

#: adduser:588 adduser:604 adduser:704 adduser:818 adduser:824
#, perl-format
msgid "The user `%s' was not created."
msgstr ""

#: adduser:601
#, perl-format
msgid "No UID is available in the range %d-%d (FIRST_SYS_UID - LAST_SYS_UID)."
msgstr ""

#: adduser:612
msgid "Neither ingroup option nor gid given."
msgstr ""

#: adduser:624
msgid "Neither ingroup option nor gid given and make_group_also unset."
msgstr ""

#: adduser:628
#, perl-format
msgid "Adding system user `%s' (UID %d) ..."
msgstr ""

#: adduser:632
#, perl-format
msgid "Adding new group `%s' (GID %d) ..."
msgstr ""

#: adduser:643 adduser:889
#, perl-format
msgid "The home dir %s you specified already exists.\n"
msgstr ""

#: adduser:646 adduser:892
#, perl-format
msgid "The home dir %s you specified can't be accessed: %s\n"
msgstr ""

#: adduser:650
#, perl-format
msgid "Adding new user `%s' (UID %d) with group `%s' ..."
msgstr ""

#: adduser:703
msgid ""
"USERS_GID and USERS_GROUP both given in configuration. This is an error."
msgstr ""

#: adduser:794
#, perl-format
msgid "Adding user `%s' ..."
msgstr ""

#: adduser:817
#, perl-format
msgid "No UID/GID pair is available in the range %d-%d (FIRST_UID - LAST_UID)."
msgstr ""

#: adduser:823
msgid ""
"USERGROUPS=no, USER_GID=-1 and USERS_GROUP empty. A user needs a primary "
"group!"
msgstr ""

#: adduser:863
msgid "Internal error interpreting parameter combination"
msgstr ""

#: adduser:874
#, perl-format
msgid "Adding new group `%s' (%d) ..."
msgstr ""

#: adduser:877
#, perl-format
msgid "Adding new group `%s' (new group ID) ..."
msgstr ""

#: adduser:880
#, perl-format
msgid "new group '%s' created with GID %d"
msgstr ""

#: adduser:899
#, perl-format
msgid "Adding new user `%s' (%d) with group `%s (%d)' ..."
msgstr ""

#: adduser:936
msgid "Permission denied"
msgstr ""

#: adduser:938
msgid "invalid combination of options"
msgstr ""

#: adduser:940
msgid "unexpected failure, nothing done"
msgstr ""

#: adduser:942
msgid "unexpected failure, passwd file missing"
msgstr ""

#: adduser:944
msgid "passwd file busy, try again"
msgstr ""

#: adduser:946
msgid "invalid argument to option"
msgstr ""

#: adduser:948
msgid "wrong password given or password retyped incorrectly"
msgstr ""

#: adduser:950
#, perl-format
msgid "unexpected return code %s given from passwd"
msgstr ""

#: adduser:957
msgid "Try again? [y/N] "
msgstr ""

#: adduser:983
msgid "Is the information correct? [Y/n] "
msgstr ""

#: adduser:997
#, perl-format
msgid "Adding new user `%s' to supplemental / extra groups `%s' ..."
msgstr ""

#: adduser:1021
#, perl-format
msgid "Setting quota for user `%s' to values of user `%s' ..."
msgstr ""

#: adduser:1059
#, perl-format
msgid "Not creating `%s'."
msgstr ""

#: adduser:1061
#, perl-format
msgid "Not creating home directory `%s' as requested."
msgstr ""

#: adduser:1063
#, perl-format
msgid "The home directory `%s' already exists.  Not touching this directory."
msgstr ""

#: adduser:1069
#, perl-format
msgid ""
"Warning: The home directory `%s' does not belong to the user you are "
"currently creating."
msgstr ""

#: adduser:1072
#, perl-format
msgid "Creating home directory `%s' ..."
msgstr ""

#: adduser:1075
#, perl-format
msgid "Couldn't create home directory `%s': %s."
msgstr ""

#: adduser:1089
#, perl-format
msgid "Copying files from `%s' ..."
msgstr ""

#: adduser:1092
#, perl-format
msgid "fork for `find' failed: %s"
msgstr ""

#: adduser:1203
#, perl-format
msgid "The user `%s' already exists, and is not a system user."
msgstr ""

#: adduser:1212
#, perl-format
msgid "The user `%s' already exists."
msgstr ""

#: adduser:1225
#, perl-format
msgid "The GID %d is already in use."
msgstr ""

#: adduser:1235
#, perl-format
msgid "No group with GID %d found."
msgstr ""

#: adduser:1304
#, perl-format
msgid "%s/%s is neither a dir, file, nor a symlink."
msgstr ""

#: adduser:1337
msgid ""
"To avoid ambiguity with numerical UIDs, usernames which\n"
"            resemble numbers or negative numbers are not allowed."
msgstr ""

#: adduser:1345
msgid "Usernames must not be a single or a double period."
msgstr ""

#: adduser:1352
msgid "Usernames must be no more than 32 bytes in length."
msgstr ""

#: adduser:1359
msgid ""
"To avoid problems, the username must not start with a\n"
"            dash, plus sign, or tilde, and it must not contain any of the\n"
"            following: colon, comma, slash, or any whitespace characters\n"
"            including spaces, tabs, and newlines."
msgstr ""

#: adduser:1374
msgid ""
"To avoid problems, the username should consist only of\n"
"            letters, digits, underscores, periods, at signs and dashes, and\n"
"            not start with a dash (as defined by IEEE Std 1003.1-2001). For\n"
"            compatibility with Samba machine accounts, $ is also supported\n"
"            at the end of the username.  (Use the `--allow-all-names' "
"option\n"
"            to bypass this restriction.)"
msgstr ""

#: adduser:1384
msgid "Allowing use of questionable username."
msgstr ""

#: adduser:1386
#, perl-format
msgid ""
"Please enter a username matching the regular expression\n"
"            configured via the %s configuration variable.  Use the\n"
"            `--allow-bad-names' option to relax this check or reconfigure\n"
"            %s in configuration."
msgstr ""

#: adduser:1415
#, perl-format
msgid "Selecting UID from range %d to %d ...\n"
msgstr ""

#: adduser:1438
#, perl-format
msgid "Selecting GID from range %d to %d ..."
msgstr ""

#: adduser:1462
#, perl-format
msgid "Selecting UID/GID from range %d to %d ..."
msgstr ""

#: adduser:1504
#, perl-format
msgid "Removing directory `%s' ..."
msgstr ""

#: adduser:1508 deluser:432
#, perl-format
msgid "Removing user `%s' ..."
msgstr ""

#: adduser:1512 deluser:474
#, perl-format
msgid "Removing group `%s' ..."
msgstr ""

#: adduser:1522
#, perl-format
msgid "Caught a SIG%s."
msgstr ""

#: adduser:1528
#, perl-format
msgid ""
"adduser version %s\n"
"\n"
msgstr ""

#: adduser:1529
msgid ""
"Adds a user or group to the system.\n"
"\n"
"For detailed copyright information, please refer to\n"
"/usr/share/doc/adduser/copyright.\n"
"\n"
msgstr ""

#: adduser:1535 deluser:534
msgid ""
"This program is free software; you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation; either version 2 of the License, or (at\n"
"your option) any later version.\n"
"\n"
"This program is distributed in the hope that it will be useful, but\n"
"WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n"
"General Public License, /usr/share/common-licenses/GPL, for more details.\n"
msgstr ""

#: adduser:1549
msgid ""
"adduser [--uid id] [--firstuid id] [--lastuid id]\n"
"        [--gid id] [--firstgid id] [--lastgid id] [--ingroup group]\n"
"        [--add-extra-groups] [--shell shell]\n"
"        [--comment comment] [--home dir] [--no-create-home]\n"
"        [--allow-all-names] [--allow-bad-names]\n"
"        [--disabled-password] [--disabled-login]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        user\n"
"    Add a regular user\n"
"\n"
"adduser --system\n"
"        [--uid id] [--group] [--ingroup group] [--gid id]\n"
"        [--shell shell] [--comment comment] [--home dir] [--no-create-home]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        user\n"
"   Add a system user\n"
"\n"
"adduser --group\n"
"        [--gid ID] [--firstgid id] [--lastgid id]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        group\n"
"addgroup\n"
"        [--gid ID] [--firstgid id] [--lastgid id]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        group\n"
"   Add a user group\n"
"\n"
"addgroup --system\n"
"        [--gid id]\n"
"        [--conf file] [--quiet] [--verbose] [--debug]\n"
"        group\n"
"   Add a system group\n"
"\n"
"adduser USER GROUP\n"
"   Add an existing user to an existing group\n"
msgstr ""

#: deluser:162
msgid "Only root may remove a user or group from the system."
msgstr ""

#: deluser:236
msgid ""
"In order to use the --remove-home, --remove-all-files, and --backup "
"features, you need to install the `perl' package. To accomplish that, run "
"apt-get install perl."
msgstr ""

#: deluser:277
#, perl-format
msgid "The user `%s' is not a system user. Exiting."
msgstr ""

#: deluser:281
#, perl-format
msgid "The user `%s' does not exist, but --system was given. Exiting."
msgstr ""

#: deluser:293
msgid ""
"WARNING: You are just about to delete the root account (uid 0). Usually this "
"is never required as it may render the whole system unusable. If you really "
"want this, call deluser with parameter --no-preserve-root. Stopping now "
"without having performed any action"
msgstr ""

#: deluser:305
msgid "remove_home or remove_all_files beginning"
msgstr ""

#: deluser:306
msgid "Looking for files to backup/remove ..."
msgstr ""

#: deluser:311
#, perl-format
msgid "failed to open /proc/mounts: %s"
msgstr ""

#: deluser:324
#, perl-format
msgid "failed to close /proc/mounts: %s"
msgstr ""

#: deluser:354
#, perl-format
msgid "Not backing up/removing `%s', it is a mount point."
msgstr ""

#: deluser:361
#, perl-format
msgid "Not backing up/removing `%s', it matches %s."
msgstr ""

#: deluser:376
#, perl-format
msgid "Cannot handle special file %s"
msgstr ""

#: deluser:385
#, perl-format
msgid "Backing up %d files to be removed to %s ..."
msgstr ""

#: deluser:408
msgid "Removing files ..."
msgstr ""

#: deluser:417
msgid "Removing crontab ..."
msgstr ""

#: deluser:424 deluser:427
#, perl-format
msgid "`%s' not executed. Skipping crontab removal. Package `cron' required."
msgstr ""

#: deluser:457
#, perl-format
msgid "getgrnam `%s' failed: %s. This shouldn't happen."
msgstr ""

#: deluser:463
#, perl-format
msgid "The group `%s' is not a system group. Exiting."
msgstr ""

#: deluser:467
#, perl-format
msgid "The group `%s' is not empty!"
msgstr ""

#: deluser:486
#, perl-format
msgid "The user `%s' does not exist.\n"
msgstr ""

#: deluser:490
#, perl-format
msgid "The group `%s' does not exist.\n"
msgstr ""

#: deluser:494
msgid "You may not remove the user from their primary group."
msgstr ""

#: deluser:512
#, perl-format
msgid "The user `%s' is not a member of group `%s'."
msgstr ""

#: deluser:516
#, perl-format
msgid "Removing user `%s' from group `%s' ..."
msgstr ""

#: deluser:527
#, perl-format
msgid ""
"deluser version %s\n"
"\n"
msgstr ""

#: deluser:528
msgid ""
"Removes users and groups from the system.\n"
"\n"
"For detailed copyright information, please refer to\n"
"/usr/share/doc/adduser/copyright.\n"
"\n"
msgstr ""

#: deluser:548
msgid ""
"deluser [--system] [--remove-home] [--remove-all-files] [--backup]\n"
"        [--backup-to dir] [--backup-suffix str] [--conf file]\n"
"        [--quiet] [--verbose] [--debug] user\n"
"\n"
"  remove a regular user from the system\n"
"\n"
"deluser --group [--system] [--only-if-empty] [--conf file] [--quiet]\n"
"        [--verbose] [--debug] group\n"
"delgroup [--system] [--only-if-empty] [--conf file] [--quiet]\n"
"         [--verbose] [--debug] group\n"
"  remove a group from the system\n"
"\n"
"deluser [--conf file] [--quiet] [--verbose] [--debug] user group\n"
"  remove the user from a group\n"
msgstr ""

#: deluser:594 deluser:604
#, perl-format
msgid "Backup suffix %s unavailable, using gzip."
msgstr ""

#: debian/tests/lib/AdduserTestsCommon.pm:421
#, perl-format
msgid "Could not find program named `%s' in $PATH.\n"
msgstr ""

#: AdduserCommon.pm:170
#, perl-format
msgid "`%s' does not exist. Using defaults."
msgstr ""

#: AdduserCommon.pm:176
#, perl-format
msgid "cannot open configuration file %s: `%s'\n"
msgstr ""

#: AdduserCommon.pm:186 AdduserCommon.pm:258 AdduserCommon.pm:286
#, perl-format
msgid "Couldn't parse `%s', line %d."
msgstr ""

#: AdduserCommon.pm:191
#, perl-format
msgid "Unknown variable `%s' at `%s', line %d."
msgstr ""

#: AdduserCommon.pm:224
#, perl-format
msgid "Cannot read directory `%s'"
msgstr ""

#: AdduserCommon.pm:239
#, perl-format
msgid "`%s' does not exist."
msgstr ""

#: AdduserCommon.pm:244
#, perl-format
msgid "Cannot open pool file %s: `%s'"
msgstr ""

#: AdduserCommon.pm:299
#, perl-format
msgid "Illegal pool type `%s' reading `%s'."
msgstr ""

#: AdduserCommon.pm:303
#, perl-format
msgid "Duplicate name `%s' at `%s', line %d."
msgstr ""

#: AdduserCommon.pm:307
#, perl-format
msgid "Duplicate ID `%s' at `%s', line %d."
msgstr ""

#: AdduserCommon.pm:345
#, perl-format
msgid ""
"`%s' refused the given user name, but --allow-all-names is given. "
"Continueing."
msgstr ""

#: AdduserCommon.pm:348
#, perl-format
msgid ""
"`%s' refused the given user name. This is a bug in adduser. Please file a "
"bug report."
msgstr ""

#: AdduserCommon.pm:352 AdduserCommon.pm:366
#, perl-format
msgid "`%s' returned error code %d. Exiting."
msgstr ""

#: AdduserCommon.pm:356 AdduserCommon.pm:369
#, perl-format
msgid "`%s' exited from signal %d. Exiting."
msgstr ""

#: AdduserCommon.pm:381
#, perl-format
msgid "`%s' failed to execute. %s. Continuing."
msgstr ""

#: AdduserCommon.pm:383
#, perl-format
msgid "`%s' killed by signal %d. Continuing."
msgstr ""

#: AdduserCommon.pm:385
#, perl-format
msgid "`%s' failed with status %d. Continuing."
msgstr ""

#: AdduserCommon.pm:427
#, perl-format
msgid "Could not find program named `%s' in $PATH."
msgstr ""

#: AdduserCommon.pm:504
#, perl-format
msgid "could not open lock file %s!"
msgstr ""

#: AdduserCommon.pm:510
msgid "Could not obtain exclusive lock, please try again shortly!"
msgstr ""

#: AdduserCommon.pm:513
msgid "Waiting for lock to become available..."
msgstr ""

#: AdduserCommon.pm:520
#, perl-format
msgid "could not seek - %s!"
msgstr ""

#: AdduserCommon.pm:529
msgid "could not find lock file!"
msgstr ""

#: AdduserCommon.pm:534
#, perl-format
msgid "could not unlock file %s: %s"
msgstr ""

#: AdduserCommon.pm:539
#, perl-format
msgid "could not close lock file %s: %s"
msgstr ""

#: AdduserLogging.pm:158
#, perl-format
msgid "logging to syslog failed: command line %s returned error: %s\n"
msgstr ""
